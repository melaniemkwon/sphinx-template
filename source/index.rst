.. TestDoc documentation master file, created by
   sphinx-quickstart on Thu Jun 27 15:46:21 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Minimal Docs
===================================

You can mix and match source formats.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   sample_doc
   markdown_doc
   notebook

.. image:: _static/images/ascus.jpg
    :align: center

.. warning::

    ``nbgitpuller`` will automagically resolve merge conflicts

.. code:: python

    print("hello world")

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
